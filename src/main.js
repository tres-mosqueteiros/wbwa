import Vue from 'vue'
import app from './App.vue'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueMaterial);


Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCFavGYhDKPwejBo1ATRV3LATx0FHlmkfk',
    libraries: "places"
  }
})

Vue.config.productionTip = false

new Vue({
  render: h => h(app)
}).$mount('#app')
